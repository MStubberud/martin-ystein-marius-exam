#pragma once

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

struct PointLight
{
	glm::vec3 position;

	float constant;
	float linear;
	float quadratic;

	glm::vec3 diffuseColor;
	glm::vec3 specularColor;
};

class LightHandler
{
public:
	LightHandler();

	PointLight getPointLight() { return pointLight; };

	void setPointLightAt(glm::vec3 const position, const glm::vec3& color = glm::vec3(1.0f));

private:
	PointLight pointLight;
};