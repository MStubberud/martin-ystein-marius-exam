#include "Camera.h"
#include "WindowHandler.h"

Camera::Camera(const glm::vec3& position, const glm::vec3& front)
{
	up_ = glm::vec3(0.0f, 1.0f, 0.0f);
	position_ = position;
	front_ = front;

	setProjection(0.1f, 200.0f);
}

void Camera::setProjection(float near, float far, float fov) 
{
	projectionMatrix_ =  glm::perspective(fov, WindowHandler::getInstance().getScreenSize().x / WindowHandler::getInstance().getScreenSize().y, near, far);
}

glm::mat4 Camera::getViewMatrix() 
{
	return glm::lookAt(position_, front_ + position_, up_);
}