#pragma once

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

class Camera 
{
public:
	Camera(const glm::vec3& position, const glm::vec3& front);
	
	void setProjection(float near, float far, float fov = glm::radians(70.f));

	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix() { return projectionMatrix_; };
	glm::vec3 getPosition() { return position_; };

private:
	glm::mat4 projectionMatrix_;

	glm::vec3 position_;
	glm::vec3 front_;
	glm::vec3 up_;
};