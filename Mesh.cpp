#include "Mesh.h"

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const Texture& texture)
{
	vertices_ = vertices;
	indices_ = indices;
	texture_ = texture;
	shininess_ = 1.0f;

	setupMesh();
}

void Mesh::draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler, glm::mat4* const modelMatrix)
{
	glm::mat4 MVPMatrix = camera->getProjectionMatrix() * camera->getViewMatrix() * (*modelMatrix);

	PointLight pointLight = lightHandler->getPointLight();

	//Bind program
	glUseProgram(shaderProgram->programId);

	//Bind Cube
	glBindVertexArray(VAO_);

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(shaderProgram->MVPId, 1, GL_FALSE, glm::value_ptr(MVPMatrix));		//Note that glm::value_ptr(gMVPMatrix) is the same as &gMVPMatrix[0][0]]
	glUniformMatrix4fv(shaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(*modelMatrix));

	//Send in light and color information
	glUniform3f(shaderProgram->cameraPositionId, camera->getPosition().x, camera->getPosition().y, camera->getPosition().z);

	glUniform1f(shaderProgram->shininessId, shininess_);

	glUniform3f(shaderProgram->pointLightPositionId, 	pointLight.position.x, 		pointLight.position.y, 		pointLight.position.z);
	glUniform1f(shaderProgram->pointLightConstantId, 	pointLight.constant);
	glUniform1f(shaderProgram->pointLightLinearId, 		pointLight.linear);
	glUniform1f(shaderProgram->pointLightQuadraticId, 	pointLight.quadratic);
	glUniform3f(shaderProgram->pointLightDiffuseId, 	pointLight.diffuseColor.x, 	pointLight.diffuseColor.y, 	pointLight.diffuseColor.z);
	glUniform3f(shaderProgram->pointLightSpecularId, 	pointLight.specularColor.x, pointLight.specularColor.y, pointLight.specularColor.z);

	glBindTexture(GL_TEXTURE_2D, texture_.id);

	//Draw Cube
	glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, NULL);

	glBindTexture(GL_TEXTURE_2D, 0);

	//Unbind program
	glUseProgram(0);
}

void Mesh::setupMesh()
{
	glGenVertexArrays(1, &VAO_);
	glGenBuffers(1, &VBO_);
	glGenBuffers(1, &IBO_);

	glBindVertexArray(VAO_);
	
	glBindBuffer(GL_ARRAY_BUFFER, VBO_);
	glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(Vertex), &vertices_[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(GLuint), &indices_[0], GL_STATIC_DRAW);

	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	// Vertex normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

	// Vertex texture coordinates
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoords));	

	glBindVertexArray(0);
}
