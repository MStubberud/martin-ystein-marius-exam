#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include <iostream>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "ShaderHandler.h"
#include "ModelHandler.h"
#include "LightHandler.h"

#include "GameEvent.h"
#include "ScreenObject.h"
#include "Camera.h"

bool initGL()
{
	//Success flag
	bool success = true;

	// Enable culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	return success;
}

bool init(bool& running) 
{

	if(!WindowHandler::getInstance().init()) 
	{
		running = false;
		return false;
	}

	SDL_WarpMouseInWindow(WindowHandler::getInstance().getWindow(), WindowHandler::getInstance().getScreenSize().x / 2.0f, WindowHandler::getInstance().getScreenSize().y / 2.0f);
	InputHandler::getInstance().init(&running);

	initGL();

	return true;
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue) 
{
	GameEvent nextEvent;
	while(!eventQueue.empty()) 
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();
	}
}


void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, ScreenObject* testCube, LightHandler* lightHandler) 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	testCube->draw(camera, shaderProgram, lightHandler);

	// Update screen
	SDL_GL_SwapWindow(WindowHandler::getInstance().getWindow());
}

// Calls cleanup code on program exit.
void close() 
{
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[]) 
{
	float fpsGoal 		 = 60.0f;
	float nextFrame      = 1/fpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime      = 0.0f; //Time since last pass through of the game loop.
	auto clockStart      = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop       = clockStart;
	bool running 		 = true;

	init(running);

	// Main event queue for the program.
	std::queue<GameEvent> eventQueue; 

	ModelHandler modelHandler;
	modelHandler.loadModelData("testCube", "./resources/models/testCube.obj");

	ScreenObject testCube(modelHandler.getModel("testCube"), glm::vec3(0.0f), glm::vec3(1.0f));

	ShaderHandler::ShaderProgram* currentShaderProgram = nullptr;
	ShaderHandler shaderHandler;
	currentShaderProgram = shaderHandler.initializeShaders();

	LightHandler lightHandler;
	lightHandler.setPointLightAt(glm::vec3(3.0f));

	Camera* activeCamera = new Camera(glm::vec3(0.0f, 0.0f, 10.0f), glm::vec3(0.0f, 0.0f, -1.0f));

	while(running) 
	{
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue);

		if(nextFrameTimer >= nextFrame) 
		{
			draw(activeCamera, currentShaderProgram, &testCube, &lightHandler);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}