#include "LightHandler.h"

LightHandler::LightHandler()
{
	pointLight.constant = 1.0f;
	pointLight.linear = 0.09f;
	pointLight.quadratic = 0.032f;
	pointLight.diffuseColor = glm::vec3(1.0f);
	pointLight.specularColor = glm::vec3(1.0f);	

}

void LightHandler::setPointLightAt(glm::vec3 const position, const glm::vec3& color)
{
	pointLight.position = position;
	pointLight.diffuseColor = color;
	pointLight.specularColor = color;
}