#include "ScreenObject.h"

ScreenObject::ScreenObject(Model* const model, const glm::vec3& position, const glm::vec3& hitbox, const glm::vec3& scale) 
{
	model_ = model;
	position_ = position;
	hitbox_ = hitbox;
	modelMatrix_ = glm::translate(glm::mat4(1.0f), position_);
	modelMatrix_ = glm::scale(modelMatrix_, scale);
}

void ScreenObject::draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler) 
{
	model_->draw(camera, shaderProgram, lightHandler, &modelMatrix_);
}