#include "Movable.h"

Movable::Movable(Model* const model, const glm::vec3& position, const glm::vec3& hitbox, const glm::vec3& scale, float rotation) 
	: ScreenObject(model, position, hitbox, scale) 
{
	shader_ = nullptr;
	speed_ = 5.0f;
	scale_ = scale;
	movementDirection_ = glm::vec3(0.0f, 0.0f, 0.0f);
	rotationDirection_ = movementDirection_;
	rotation_ = rotation;
	rotationSpeed_ = 10.0f;
}

void Movable::move(float deltaTime)
{
	position_ = position_ + (movementDirection_ * deltaTime * speed_);

	if (movementDirection_ != rotationDirection_)
	{
		rotate(deltaTime);
	}
}

void Movable::rotate(float deltaTime) 
{
	float angle = glm::cross(movementDirection_, rotationDirection_).y;
	if (angle < -rotationSpeed_ * deltaTime * 0.5)
	{
		rotation_ -= rotationSpeed_ * deltaTime;
	}
	else if (angle > rotationSpeed_ * deltaTime * 0.5)
	{
		rotation_ += rotationSpeed_ * deltaTime;
	}
	else 
	{
		// Gives pacman nudge if he turns 180 degrees
		angle = glm::acos(glm::dot(movementDirection_, rotationDirection_));
		if (angle > glm::radians(160.0f))
		{
			rotation_ += rotationSpeed_ * deltaTime;
		}
	}
	rotationDirection_ = glm::vec3(glm::cos(rotation_), 0.0f, glm::sin(rotation_));
}

void Movable::draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler)
{
	modelMatrix_ = glm::translate(glm::mat4(1.0f), position_);
	modelMatrix_ = glm::rotate(modelMatrix_, -rotation_, glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrix_ = glm::scale(modelMatrix_, scale_);

	if (shader_)
	{
		ScreenObject::draw(camera, shader_, lightHandler);
	}
	else
	{
		ScreenObject::draw(camera, shaderProgram, lightHandler);
	}
}


void Movable::setDirection(Movable::direction direction) 
{
 	oldMovementDirection_ = movementDirection_;
	oldDirection_ = direction_;
 
 	switch(direction) 
 	{
 		case(UP):
			movementDirection_ = glm::vec3(0.0f, 0.0f, -1.0f);
 			break;
 		case(LEFT):
 			movementDirection_ = glm::vec3(-1.0f, 0.0f, 0.0f);
 			break;
 		case(DOWN):
			movementDirection_ = glm::vec3(0.0f, 0.0f, 1.0f);
 			break;
 		case(RIGHT):
 			movementDirection_ = glm::vec3(1.0f, 0.0f, 0.0f);
			break;
		case(NONE):
			movementDirection_ = glm::vec3(0.0f, 0.0f, 0.0f);
 			break;
		default:
			printf("Error!: The direction is not valid.\n");
	}
	direction_ = direction;
}