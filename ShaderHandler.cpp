#include "ShaderHandler.h"

const std::string shaderPath = "./resources/shaders/";

ShaderHandler::ShaderProgram* ShaderHandler::initializeShaders()
{
	shaders["pointLight"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("pointLight"));

	return shaders["pointLight"].get();
}

ShaderHandler::ShaderProgram* ShaderHandler::getShader(const std::string& shader)
{
	auto it = shaders.find(shader);
	if (it != shaders.end())
	{
		return shaders[shader].get();
	}

	return nullptr;
}

ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";

	programId 		= LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str());
	
	MVPId 			= glGetUniformLocation(programId, "MVP");
	modelMatrixId 	= glGetUniformLocation(programId, "modelMatrix");

	textureId 	= glGetUniformLocation(programId, "material.texture");
	shininessId = glGetUniformLocation(programId, "material.shininess");

	pointLightPositionId 		= glGetUniformLocation(programId, "pointLight.position");
	pointLightConstantId 		= glGetUniformLocation(programId, "pointLight.constant");
	pointLightLinearId 			= glGetUniformLocation(programId, "pointLight.linear");
	pointLightQuadraticId 		= glGetUniformLocation(programId, "pointLight.quadratic");
	pointLightDiffuseId 		= glGetUniformLocation(programId, "pointLight.diffuse");
	pointLightSpecularId 		= glGetUniformLocation(programId, "pointLight.specular");

	cameraPositionId 	= glGetUniformLocation(programId, "cameraPos");

	glUseProgram(programId);
}

ShaderHandler::ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(programId);
}