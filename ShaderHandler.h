#pragma once


#include <unordered_map>
#include <memory>

#if defined(__linux__)						// If we are using linux.

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include "shader.h"

#include <string>


// Handles loading multiple shaderprogram and switching between them.
class ShaderHandler
{
public:
	class ShaderProgram
	{
	public:
		ShaderProgram(const std::string& shader);
		~ShaderProgram();

		GLuint programId;
		GLuint MVPId;
		GLuint modelMatrixId;

		GLuint textureId;
		GLuint shininessId;

		GLuint pointLightPositionId;
		GLuint pointLightConstantId;
		GLuint pointLightLinearId;
		GLuint pointLightQuadraticId;
		GLuint pointLightDiffuseId;
		GLuint pointLightSpecularId;

		GLuint cameraPositionId;
	};

	ShaderProgram* initializeShaders();


	ShaderProgram* getShader(const std::string& shader);

	ShaderProgram* setShader(const std::string& shader);

private:

	std::unordered_map<std::string, std::unique_ptr<ShaderProgram>> shaders;
};

